# JFinalShop

## 项目介绍
由于白衣同学不再维护JFinalShop项目，特此弄个新分支，且使用Idea 社区版部署。

## 项目编辑、修改、开发

本项目想要在idea启动起来，请先下载smart tomcat，如不会使用请自行Google。

## 项目部署

请使用命令打包工程。

``` shell
# 进入工程目录
cd JFinalShop/WebRoot
## 打包
jar cvf /tmp/ROOT.war *

## 把ROOT.war放到tomcat webapps目录
cp /tmp/ROOT.war tomcat/webapps
```

