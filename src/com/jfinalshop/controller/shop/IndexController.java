package com.jfinalshop.controller.shop;

import cn.dreampie.captcha.CaptchaRender;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import com.jfinal.ext.route.ControllerBind;


/**
 * 前台类 - 首页
 * 
 */
@ControllerBind(controllerKey = "/")
public class IndexController extends Controller{
	
	// 首页
	public void index(){
//		String path = null;
//		try {
//			path = PathKit.class.getClassLoader().getResource("").toURI().getPath();
//			System.out.println("路径是："+path);
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
//		if(path != null){
//			String[] ul = path.split("WEB-INF");
//			path = ul[0];
//			path = path.substring(1,path.length());
//			System.out.println("最终的路径是："+path);
//		}
//		String outPath = getPgPath().toString();
//		List<String> list = getFileList(path+"upload/banner/",outPath+"upload/banner/");
//		System.out.println("返回首页的所有数据："+list.toString());
//		setAttr("bannerList", list);
//		render("index.html");
		redirect("/index.html");
	}
	
	public String getRoPath() throws URISyntaxException, IOException{
		String path = Class .class.getResource("/").toURI().getPath();
        return new File(path).getParentFile().getParentFile().getCanonicalPath();
	}
	
	public StringBuffer getPgPath(){
		return getRequest().getRequestURL();
	}
	
	public static List<String> getFileList(String strPath,String outPath) {
		List<String> list = new ArrayList<String>();
        File dir = new File(strPath);
        System.out.println("第一个File:"+dir.toString());
        File[] files = dir.listFiles(); // 该文件目录下文件全部放入数组
        System.out.println("第二个File:"+files.toString());
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                String fileName = files[i].getName();
                if (files[i].isDirectory()) { // 判断是文件还是文件夹
                    getFileList(files[i].getAbsolutePath(),outPath); // 获取文件绝对路径
                } else {
                	String strFileName = files[i].getAbsolutePath();
                    System.out.println("---" + strFileName);
                    list.add(outPath+files[i].getName());
                }
            }
        }
        return list;
    }
	
	/**
	 * 验证码
	 */
	@Clear
	public void captcha() {
		int width = 0, height = 0, minnum = 0, maxnum = 0, fontsize = 0;
		CaptchaRender captcha = new CaptchaRender();
		if (isParaExists("width")) {
			width = getParaToInt("width");
		}
		if (isParaExists("height")) {
			height = getParaToInt("height");
		}
		if (width > 0 && height > 0)
			captcha.setImgSize(width, height);
		if (isParaExists("minnum")) {
			minnum = getParaToInt("minnum");
		}
		if (isParaExists("maxnum")) {
			maxnum = getParaToInt("maxnum");
		}
		if (minnum > 0 && maxnum > 0)
			captcha.setFontNum(minnum, maxnum);
		if (isParaExists("fontsize")) {
			fontsize = getParaToInt("fontsize");
		}
		if (fontsize > 0)
			captcha.setFontSize(fontsize, fontsize);
		// 干扰线数量 默认0
		captcha.setLineNum(1);
		// 噪点数量 默认50
		captcha.setArtifactNum(10);
		// 使用字符 去掉0和o 避免难以确认
		captcha.setCode("123456789");
		 //验证码在session里的名字 默认 captcha,创建时间为：名字_time
		// captcha.setCaptchaName("captcha");
	    //验证码颜色 默认黑色
		// captcha.setDrawColor(new Color(255,0,0));
	    //背景干扰物颜色  默认灰
		// captcha.setDrawBgColor(new Color(0,0,0));
	    //背景色+透明度 前三位数字是rgb色，第四个数字是透明度  默认透明
		// captcha.setBgColor(new Color(225, 225, 0, 100));
	    //滤镜特效 默认随机特效 //曲面Curves //大理石纹Marble //弯折Double //颤动Wobble //扩散Diffuse
		captcha.setFilter(CaptchaRender.FilterFactory.Curves);
		// 随机色 默认黑验证码 灰背景元素
		captcha.setRandomColor(true);
		render(captcha);
	}
}
